import 'antd/dist/antd.css';
import './App.css';
import { Table, Divider, Select, Button } from 'antd';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
const { Option } = Select;

function App() {
  const columns = [
    {
      title: 'Customer Name',
      dataIndex: 'customerName',
      key: 'customerName',
      render: text => <a>{text}</a>,
    },
    {
      title: 'Country Code',
      dataIndex: 'countryName',
      key: 'countryName',
    },

    {
      title: 'Phone',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'State',
      dataIndex: 'phoneState',
      key: 'phoneState',
    },

  ];

  const [data, setData] = useState([]);
  const [searchParams, setSearchParams] = useState({});


  useEffect(() => {
    axios.get('http://localhost:8080/api/customer').then(resp => {
      setData(resp.data);
    });
  }, []);

  const reset = () => {
    axios.get('http://localhost:8080/api/customer').then(resp => {
      setData(resp.data);
    });
  }

  const filterCountry = (val) => {
    let params = {
      ...searchParams,
      countryName: val
    };
    setSearchParams(params);
    axios.get('http://localhost:8080/api/customer', { params }).then(resp => {
      setData(resp.data);
    });
  }
  const filterState = (val) => {
    let params = {
      ...searchParams,
      isValid: val
    };
    setSearchParams(params);
    axios.get('http://localhost:8080/api/customer', { params }).then(resp => {
      setData(resp.data);
    });
  }

  return (
    <div className="App">

      <div style={{ float: 'left', padding: '5px' }}>
        Country
        &nbsp;&nbsp;
        <Select placeholder="Filter By state " style={{ width: 150 }} onSelect={filterCountry} >
          <Option value='Morocco'>Morocco</Option>
          <Option value='Cameroon'>Cameroon</Option>
          <Option value='Ethiopia'>Ethiopia</Option>
          <Option value='Mozabique'>Mozabique</Option>
          <Option value='Uganda'>Uganda</Option>
        </Select>

        &nbsp;&nbsp;
        State
        &nbsp;&nbsp;
        <Select placeholder="Filter By Country " showSearch style={{ width: 150 }} onSelect={filterState} >
          <Option value={null}>All</Option>
          <Option value={true}>Valid</Option>
          <Option value={false}>In-Valid</Option>
        </Select>
      </div>
      &nbsp;&nbsp;
      <Button type='primary' onClick={() => reset()} >Reset</Button>
      <Divider />
      <Table columns={columns} dataSource={data} />
    </div>
  );
}

export default App;
