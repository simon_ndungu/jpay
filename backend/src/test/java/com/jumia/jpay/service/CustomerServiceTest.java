package com.jumia.jpay.service;

import com.jumia.jpay.config.PhoneNumberPatternEnum;
import com.jumia.jpay.controller.CustomerData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CustomerServiceTest {
    @Autowired
    CustomerService customerService;

    PhoneNumberPatternEnum phoneNumberPatternEnum;

    @Test
    void isStateValid() {
        phoneNumberPatternEnum = PhoneNumberPatternEnum.Cameroon;
        String phoneNumber = "(237) 697151594";
        assertEquals(true, customerService.isStateValid(phoneNumberPatternEnum.getRegex(), phoneNumber));
    }

    @Test
    void stateInValid() {
        phoneNumberPatternEnum = PhoneNumberPatternEnum.Cameroon;
        String phoneNumber = "(237) 6622284920";
        assertEquals(false, customerService.isStateValid(phoneNumberPatternEnum.getRegex(), phoneNumber));
    }
}