package com.jumia.jpay.service;

import com.jumia.jpay.config.PhoneNumberPatternEnum;
import com.jumia.jpay.controller.CustomerData;
import com.jumia.jpay.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<CustomerData> fetchCustomerData2(String countryName, Boolean isValid) {
        String valid = isValid != null && isValid ? "VALID" : "INVALID";
        List<CustomerData> customers = customerRepository.findAll().stream()
                .map(r -> {
                    CustomerData data = r.toData();
                    String[] ar = r.getPhone().split(" ");
                    String code = ar[0];
                    code = code.replaceAll("\\(", "").replaceAll("\\)", "");
                    data.setPhone(ar[ar.length - 1]);
                    data.setCountryCode(code);
                    data.setCountryName(PhoneNumberPatternEnum.fromCode(code).name());
                    data.setPhoneState(isStateValid(PhoneNumberPatternEnum.fromCode(code).getRegex(), r.getPhone()) == true ?
                            "VALID" : "INVALID");
                    return data;
                }).filter(item -> {
                    if (countryName != null && isValid != null) {
                        return item.getCountryName().equals(countryName) &&
                                item.getPhoneState().equals(valid);
                    } else if (countryName != null) {
                        return item.getCountryName().equals(countryName);
                    } else if (isValid != null) {
                        return item.getPhoneState().equals(valid);
                    } else {
                        return true;
                    }
                }).collect(Collectors.toList());


        return customers;
    }

    public boolean isStateValid(String regex, String phoneNumber) {
        boolean isValid = false;
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(phoneNumber);

        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}