package com.jumia.jpay.controller;

import com.jumia.jpay.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer")
    public ResponseEntity<?> getCustomer(
            @RequestParam(value = "countryName", required = false) final String countryName,
            @RequestParam(value = "isValid", required = false) final Boolean isValid
    ) {
        System.out.println("START: Fetch customer data");
        List<CustomerData> response = customerService.fetchCustomerData2(countryName, isValid);

        if (CollectionUtils.isEmpty(response)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

    }

}
