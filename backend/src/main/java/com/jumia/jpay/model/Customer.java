package com.jumia.jpay.model;


import com.jumia.jpay.config.Identifier;
import com.jumia.jpay.controller.CustomerData;

import javax.persistence.Entity;

@Entity
public class Customer extends Identifier {
    private String name;
    private String phone;

    public CustomerData toData() {
        CustomerData data = new CustomerData();
        data.setCustomerName(this.getName());
        data.setPhone(this.getPhone());
        return data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}
