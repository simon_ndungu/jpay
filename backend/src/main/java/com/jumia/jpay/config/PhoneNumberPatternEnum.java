package com.jumia.jpay.config;

public enum PhoneNumberPatternEnum {
    Cameroon("237", "\\(237\\)\\ ?[2368]\\d{7,8}$"),
    Ethiopia("251", "\\(251\\)\\ ?[1-59]\\d{8}$"),
    Morocco("212", "\\(212\\)\\ ?[5-9]\\d{8}$"),
    Mozabique("258", "\\(258\\)\\ ?[28]\\d{7,8}$"),
    Uganda("256", "\\(256\\)\\ ?\\d{9}$");

    private final String code;
    private final String regex;

    PhoneNumberPatternEnum(String code, String regex) {
        this.code = code;
        this.regex = regex;
    }

    public String getCode() {
        return code;
    }

    public String getRegex() {
        return regex;
    }

    public static PhoneNumberPatternEnum fromCode(String code) {
        for (PhoneNumberPatternEnum b : PhoneNumberPatternEnum.values()) {
            if (b.getCode().equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }
}
